/* Definirajte varijablu a i b. Prilikom inicijalizacije varijabli a
dodijelite vrijednost "Dobro" a varijabli b vrijednost "vecer". Ispišite
pomocu alert naredbe "Dobro vecer" */

var a = 'Dobro';
var b = 'vecer';
alert(a + ' ' + b);

/* Ispišite tekst "Marko je išao u školu. Iva je bila u kazalištu." Tako da
prva recenica bude u jednom redu a druga recenica u drugom redu. */

console.log("Marko je išao u školu.\nIva je bila u kazalištu.");

/* Ispišite tekst "Marko je išao u školu. Iva je bila u kazalištu." Tako da
prva recenica bude odvojena od druge recenice tab-om. */

console.log("Marko je išao u školu.\tIva je bila u kazalištu.");

/* Ispišite tocno ovakav tekst:
Marko: Danas sam dobio jedinicu iz matematike!
Iva:   Pa što je bilo!?
Marko: Uciteljica je rekla da napišem "1 / 2"
Iva:   I što si napis'o?
Marko: "1 \ 2" na što se uciteljica razbjesnila */

var string = "Mako: Danas sam dobio jedinicu iz matematke\nIva: Pa što je bilo!?\nMarko: Učieljica je rekla da napišem \"1 / 2\"\nIva: I što si napisa\'o?\nMarko: \"1 / 2\" na što se uciteljica razbjesnila";

console.log(string);

/* Ispišite dužinu prethodnog teksta. */

console.log(string.length)

/* Definirajte varijablu a. Varijabli a dodijlite neku string vrijednost.
Ispišite vrijednost tako da sva slova budu velika. */


var a = 'String value';
console.log(a.toUpperCase());

/* Definirajte varijablu a. Varijabli a dodijlite neku string vrijednost.
Ispišite vrijednost tako da sva slova budu mala. */

console.log(a.toLowerCase());

/* Definirajte varijablu a. Varijabli a dodijlite neku string vrijednost.
Ispišite gdje se nalazi slovo a u varijabli. */

console.log(a.indexOf('a'));

/* Definirajte varijablu a. Varijabli a dodijlite neku string vrijednost.
Ispišite gdje se nalazi zadnje slovo a u varijabli. */

console.log(a.lastIndexOf('a'));

/* Definirajte varijablu a te joj prilikom incijalizacije dodijelite
vrijednost "Dobro vecer!". Zamjenite rijec "vecer" sa rijeci "jutro" */

var a = 'Dobro vecer!'

console.log(a.replace("vecer", "jutro"));

/* Definirajte varijablu a. Dodijlite joj neku proizvoljnu tekstualnu
vrijednost. Ispišite prvo slovo iz teksta. */

var a = 'Text value';

console.log(a.charAt(0));

/* Definirajte varijablu a. Dodijlite joj neku proizvoljnu tekstualnu
vrijednost. Ispišite trece slovo iz teksta. */


console.log(a.charAt(2));

/* Definirajte varijablu a. Dodijlite joj neku proizvoljnu tekstualnu
vrijednost. Ispišite zadnje slovo iz teksta. */

console.log(a.charAt(a.length - 1));

/* Definirajte varijablu a. Dodijlite joj neku proizvoljnu tekstualnu
vrijednost duže od 10 znakova. Ispišite tekst od 3 do 8 znaka. (substr) */


var a = 'otorinolaringologija';

console.log(a.substr(3,4));


/* Definirajte varijablu a. Dodijlite joj neku proizvoljnu tekstualnu
vrijednost duže od 10 znakova. Ispišite tekst od 3 do 8 znaka. (substring) */

console.log(a.substring(3,7));


/* Definiraj varijablu a i dodijli joj vrijednost "Zoveš se: ". Definirajte
varijablu b i pomocu prompt naredbe ucitajte korisnikovo ime. Ispišite ime
korisnika u obliku recenice koristeci varijablu a. */


var a = "Zoveš se: ";
var b = prompt("Kako se zoveš?", 'Pero')

console.log(a + ' ' + b);


/* Definirajte varijablu a, b i c. Varijabli a dodijelite vrijednost 3 a
varijabli b vrijednost 4. U varijablu c spremite zbroj varijalbe a i b.
Ispišite varijablu c sa alert naredbom. */

var a = 3;
var b = 4;
var c = a + b;
alert(c);

/* Definirajte varijablu a i b. Varijabli a dodjelite vrijednost 2.13 a
varijabli b 4. Ispišite umnožak te dvije varijable. Jel taj rezultat
cjelobrojni ili decimalni? */

var a = 2.13;
var b = 4;
console.log(a*b + "rezultat je decimalan");

/* Definirajte varijablu a i prilikom incijalizacije joj dodijelite brojcanu
neku vrijednost. Povecajte ju za jedan. Ispišite varijablu a.
 */

var a = 999;
console.log("vrijednost " + a + " je inicijalizirana , a inkrement vrijednosti je " + ++a );

/* Definirajte varijablu a. Dodijelite neku brojcanu vrijednost varijabli a.
Smanjite ju za jedan. Ispišite varijablu a. */

var a = 1000;
console.log("vrijednost " + a + " je inicijalizirana , a dekrement vrijednosti je " + --a );

/* Definirajte varijablu a. Dodijelite neku brojcanu vrijednost varijabli a.
Povecajte ju za 10 (na oba nacina). Ispišite varijablu a. */

var a = 10;
a+=10;
var b = a;

console.log("var b = a + 10 rezultat je " + b + " a, rezultat dobiven a+=10 , var b = a je " + b );

/* Definirajte varijablu a. Dodijelite neku brojcanu vrijednost varijabli a.
Smanjite ju za 10 (na oba nacina). Ispišite varijablu a. */

var a = 100;
a-=10;
var b = a;

console.log("var b = a - 10 rezultat je " + b + " a, rezultat dobiven a-=10 , var b = a je " + b );

/* Definirajte varijablu r. Pomnožite varijablu r sa 3.14 (na oba nacina).
Ispišite varijablu r. */

var r = 1;
r*=3.14;
console.log(" r = 1 , r*=3.14 je " + r);


/* Definirajte varijablu a. Definirajte joj neku vrijednost. Ispišite koja je
sljedeca (za jedan veca) vrijednost varijable a te pritom pazite da samoj
varijabli ne promjenite vrijednost (pomocu jedne naredbe, na oba nacina) */

var a = 1;
console.log(++a);
/* ???????????????? */

/* Definirajte varijablu a. Definirajte joj neku vrijednost. Ispišite koja je
prethodna (za jedan manja) vrijednost varijable a te pritom pazite da samoj
varijabli ne promjenite vrijednost (pomocu jedne naredbe, na oba nacina) */

/* Definirajte varijalbu a, b i c. Dodijelite varijablama neku brojcanu
vrijednost. Podijelite varijablu a i b te ju snimite u c varijablu. Ispišite
vrijednost c varijable */
var a = 27;
var b = 3;
var c = a / b;
console.log('27 / 3 je ' + c);


/* Definirajte varijablu a i b. Dodijelite varijablama brojcane vrijednosti.
Ispišite ostatak od djeljenja. */

var a = 28;
var b = 3;
var c = a % b;
console.log('28 % 3 je ' + c);

/* Definirajte varijablu a. Varijabli a dodijlite vrijednost pomocu prompt
naredbe. Ispišite varijablu a. */

var  a = prompt('Dodijeli mi vrijednost', 6);
console.log('Moja vrijednost je ' + a);


/* Pomocu naredbe prompt ucitajte dva broja. Prikaži zbroj ta dva broja. */

var a = prompt('Unesi prvi pribrojnik',2);
var b = prompt('Unesi drugi pribrojnik',3);
var c = parseInt(a) + parseInt(b);
console.log('Zbroj ' + a + ' + ' + b + ' = ' + c);

/* Definirajte varijablu a. Pomocu naredbe prompt ucitajte neku vrijednost u
varijablu a Pokušajte ju pretvoriti u cijeli broj te ga kvadriraj. Ispišite
kvadriranu vrijednost. */

var a = prompt('Unesi decimalni broj',5.67);
console.log('Tvoj uneseni decimalni broj je ' + a);
var b = Math.floor(a)*Math.floor(a);
console.log('Kvadrat vaše varijable nako zaokruživanja iznosi ' + b);



/* Definirajte varijablu a. Pomocu naredbe prompt ucitajte neku vrijednost u
varijablu a. Pokušajte ju pretvoriti u decimalni broj te ju zaokružite.
Ispišite broj. */

var a = prompt('Unesi decimalni broj',6,34);
console.log('Tvoj uneseni decimalni broj je ' + a);
a = Math.floor(a);
console.log('Zaokružena vrijednost vašeg broja je ' + a);


/* Pomocu naredbe prompt ucitajte neku vrijednost. Pokušajte ju pretvoriti u
decimalni broj te ju zaokružite na prvi cijeli broj. Ispišite broj. */

var a = prompt('Unesi decimalni broj s više decimala',2.4567);
a = parseFloat(a).toFixed(2);
console.log('Vaša vrijednost decimalnog broja je ' + a);
console.log('Zaokružena vrijednost vašeg decimalnog broja je ' + Math.round(a));





/* Pomocu naredbe prompt ucitajte neku vrijednost. Pokušajte ju pretvoriti u
decimalni broj te ju zaokružite na prvi sljedeci cijeli broj. Ispišite broj. */

var a = prompt('Unesite decimalni broj',3.45);
console.log('Vaša vrijednost zaokruženog sljedeceg decimalnog broja je ' + Math.ceil(a));


/* Ispiši slucajno generirani broj. */

console.log('Slučajno generirani broj je ' + Math.random().toFixed(2));


/* Definiraj varijablu a. Dodijeli varijabli a neku vrijednost. Ispiši
korijen varijable a. */

var a = 2019;
console.log('Korijen iz 2019 je ' + Math.sqrt(a));


/* Definiraj varijablu a. Pomocu prompt naredbe ucitaj vrijednost. Provjeri
jel je unesena vrijednost broj. */

var a = prompt('Unesi broj ili slovo za provjeru tipa', 'a8s45');
if ( isNaN(a)){
    console.log('Provjerom je utvrđeno da niste unjeli broj');
}else{
    console.log('Provjerom smo utvrdili da je unesen broj');   
}

/* Definirajte boolean varijablu vrijednosti true i ispišite ju. */

var a = true;
console.log('varijabla a je '+a);


/* Definirajte boolean varijablu vrijednosti false i ispišite ju. */

var a = false;
console.log('varijabla a je '+a);

/* Definirajte varijablu a koja ce biti tipa array. */

var a = [];



/* Definirajte varijablu a koja ce biti tipa array. Zatim joj dodajte 5
elemenata po izboru. (svaki element zasebna naredba, oba na cina)*/

var a = [1,2,3,4,5];

/* Definirajte varijablu a koja ce biti tipa array. Zatim joj dodajte 5
elemenata koji ce biti mapirani asocijativno (indeks je tekstualan a ne
string) po izboru. (svaki element zasebna naredba, oba nacina). Ispiši
array. */

var a = [
    {
        ime: 'Mate Mišo Kovač',
        album:'Grobovi im nikad oprostiti neće',
        godina: '1991'
    },
    {
        ime: 'Doris Dragović',
        album: 'Željo moja',
        godina: '1986'
    },
    {
        ime: 'Severina Vučković',
        album: 'Djevojka sa sela',
        godina: '1998',
    },
    {
        ime: 'Magazin',
        album: 'Put putujem',
        godina: '1986'
    },
    {
        ime: 'Oliver Dragojević',
        album: 'Skalinada 2',
        godina: '1993'
    }
];

console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Na kraj array-a dodaj novi element. Ispiši array. */

var a = [1,2,3,4,5,6];
a.push(7);
console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Na pocetak array-a dodaj novi element. Ispiši array. */

var a = [1,2,3,4,5,6];
a.unshift(0);
console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Izbaci zadnji element array-a. Ispiši array.
 */

 var a = [1,2,3,4,5,6];
 a.pop();
 console.log(a);
 

/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Izbaci prvi element array-a. Ispiši array. */

var a = [1,2,3,4,5,6];
a.shift();
console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Ispiši drugi i treci element array-a. */

var a = [1,2,3,4,5,6];
console.log(a.slice(1,3));



/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Izbaci drugi i treci element array-a. Ispiši array. */

var a = [1,2,3,4,5,6];
a.splice(1,2);
console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Sortiraj array. Ispiši array. */

var a = [1,2,4,5,6,3];
a.sort();
console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Zamjeni mjesta svim elementima array-a tako da prvi postane
zadnji a zadnji prvi. Ispiši array. */

var a = [1,2,3,4,5,6];
a.reverse();
console.log(a);


/* Definirajte varijablu a koja ce biti tipa array i dodijeli joj neke
vrijednosti. Sortiraj array (obrnuto Z-A). Ispiši array. */

var a = ['a','b','z','c','y','d','e','f'];
a.sort().reverse();
console.log(a);



/* Definirajte varijablu a koja ce biti tipa array (matrica, vektor, niz...)
a u njoj ce se nalaziti popis vaših najdražih TV serija. Ispišite tekst
"Moja nadraža serija je A i B i C..." gdje su A, B, C itd. elementi array-a */

var a = ['Breaking Bad','Peaky Blinders','Sons of Anarchy'];
console.log('Moja najdraža serija je ' + a[0]+','+a[1]+','+a[2]);


/* Definirajte varijablu a koja ce biti tipa array (matrica, vektor, niz...)
a u njoj ce se nalaziti popis vaših najdražih TV serija. Ispišite broj
elemenata. */


console.log('veličina mog arraya je ' + a.length);



/* Definirajte varijablu a koja ce biti tipa array (matrica, vektor, niz...)
a u njoj ce se nalaziti popis vaših najdražih TV serija. Ispišite zadnji
elemenat(vrijednost). */

console.log('zadnji element u mojoj matrici je ' + a.slice(-1));


/* Definirajte varijablu a koja ce biti tipa array (matrica, vektor, niz...)
a u njoj ce se nalaziti popis vaših najdražih TV serija. Ispišite zadnji
elemenat(indeks). */

console.log('index zadnjeg elementa matrie je ' + a.indexOf('Sons',0));


/* Definirajte varijablu a. Ucitajte brojcanu vrijednost pomocu naredbe
prompt. Provjerite jel unesena vrijednost veca od 5 ako je ispišite
odgovarajucu poruku. */

var a = prompt('Unesite broj koji je veći ili manji od 5' ,5);
if(a == 5){
    console.log('uneseni broj je 5');
}else if( a > 5){
    console.log('uneseni broj je veci od 5');
}else{
    console.log('uneseni broj je manji od 5');
}

/* Definirajte varijablu a. Ucitajte brojcanu vrijednost pomocu naredbe
prompt. Provjerite jel unesena vrijednost veca od 5 ako je ispišite
odgovarajucu poruku a ako ne javite korisniku da je upisao broj manji od 5. */


/*  Definirajte varijablu a. Dodjelite varijabli a slucajan broj. Provjerite
jel varijabla u intervalu do 0.25, 0.26 do 0.5, 0.51 do 0.75 ili veca od
0.76. Ispišite u kojem je rasponu varijabla a. */

var  a = prompt('unesite vrijednost u rasponu od 0 do 1', 0.77);
if ( (a >= 0.25 && a <= 0.51) | (a >= 0.51 && a <= 0.76)){
    console.log('broj je u rasponu od 0.25 do 0.67');
}else{
    console.log('broj nije u rasponu');
    
}

/*  Definirajte varijablu a. Pomocu naredbe prompt ucitajte vrijednost.
Provjerite jel upisana brojcana vrijednost i jel pozitivan. Ako je javite
korisniku da je upisao pozitivan broj inace ga obavjestite da nije upisao
pozitivan broj. */

var a = prompt('Unesite pozitivnu ili negativnu vrijednost', -1);
if(a < 0 ){
    console.log('vaš broj je ' + a + ' i on je manji od 0');
    
}else{
    console.log('vaš broj je ' + a + ' i on je veci od 0');
}

/* Definirajte varijablu a. Pomocu naredbe prompt ucitajte vrijednost.
Provjerite jel varijabla u rasponu od 1 do 10. Javite korisniku rezultat. */

var  a = prompt('Unesite broj u rasponu od 1 do 10', 10);
if(a >= 1 && a <= 10){
    console.log('vaš broj je ' + a + ' i on je u rasponu od 1 do 10');   
}else{
    'vaš broj je ' + a + ' i on niej u rasponu od 1 do 10'
}

/* Definirajte varijablu a. Pomocu naredbe prompt postavite pitanje korisniku
koji ce imati više od 2 ponudena odgovora. Provjerite korisnikov odgovor te
mu ispišite odgovarajuci tekst. */

var a = prompt('Koliko krila ima muha?\na)1 \nb)2 \nc)3');
if(a == 'b' | a == 2){
    
    console.log('vaš odogovor je točan');
    
}else{
    console.log('vaš odgovor je ne točan');
    
}







